# 自定义WSL发行版工具

## 特点

1. 与Microsoft制定的WSL发行版规范：[**WSL-DistroLauncher**](https://github.com/Microsoft/WSL-DistroLauncher) 兼容。
2. 支持从DockerHub中自动构建WSL需要的镜像。
3. 支持安装时自选版本。
4. 非应用商店安装，无需开发者模式和证书导入环节，安装更加简单与便捷。
5. 更加简单的自定义WSL发行版方式。

## 定制发行版方式

* 只需要修改launcher.json中的内容

1. `distro`为发行版注册使用的名字。自定义。需要与已存在的名字不冲突，如同时有两个发行版`centos6`和`centos7`
2. `repo`为安装时拉取系统rootfs使用的docker image库，如`library/centos`，`base/archlinux`
    
> 【注】不同的发行版需要不同的目录存放！

## 安装该发行版

1. 系统设置中开启`开发者人员模式`，注意，启用后最好重启一下
2. 新建一个用于存放发行版的目录，如`D:\XXX`
3. 从[发行版](https://gitee.com/love_linger/WSL-Launcher/releases)中下载`launcher.zip`解压到`D:\XXX\`中。
4. 修改`launcher.json`中的配置，为自己需要的发行版。
5. 以管理员运行`xxx.exe`，即可。  
    ![Preview](Preview.png)
6. 安装完成后，再次运行`xxx.exe`(不需要管理员权限)，将启动该子系统会话。

## 设置默认登录帐号

1. 在子系统中创建用户`yyy`，并配置用户权限及密码。
2. 退出子系统，命令行中输入`xxx.exe config --default-user yyy`即可。

## 卸载

    uninstall.bat