@setlocal
@echo off

@if "%1"=="-h" goto :USAGE
@if "%1"=="--help" goto :USAGE
@if "%1"=="help" goto :USAGE

@if not exist build mkdir build
@if not exist build\objs mkdir build\objs

@set CXX=cl -c -O2 -MD -nologo -DUNICODE -D_UNICODE -DNDEBUG -D_CRT_SECURE_NO_WARNINGS -DCURL_STATICLIB -EHsc
@set RES=rc -nologo
@set LD=link -nologo -dynamicbase -nxcompat -libpath:curl\lib libcurl.lib ws2_32.lib wldap32.lib advapi32.lib crypt32.lib normaliz.lib

@%CXX% -Fobuild\objs\Api.cc.obj Api.cc
@%CXX% -Fobuild\objs\Json.cc.obj Json.cc
@%CXX% -Fobuild\objs\Launcher.cc.obj Launcher.cc
@%RES% -Fobuild\objs\Launcher.rc.obj Launcher.rc
@%LD% -out:build\launcher.exe build\objs\*.obj

@goto :END

:USAGE
@echo Usage : mk name [repo]
@echo.
@echo Build launcher for WSL distribution
@echo.
@echo Options:
@echo   name    Name for custom WSL distribution
@echo   repo    Optional. Custom repo for get rootfs image from Docker Hub.
@echo.

:END